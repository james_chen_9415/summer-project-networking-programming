/*
 * UDP-IPv6-client.c
 *
 *  Created on: 03/12/2016
 *      Author: james
 */

/* $Id$
 *
 *
 *
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

#define MAXBUF 65536

int main(int argc, char* argv[])
{
   int sock;
   int status;
   struct addrinfo sainfo, *psinfo;
   struct sockaddr_in6 sin6;
   int sin6len;
   char buffer[MAXBUF];

   //Check arguements are enought or not
   if(argc < 2)
     printf("Specify a port number\n"), exit(1);

   // Create socket
   sock = socket(PF_INET6, SOCK_DGRAM,IPPROTO_UDP);

   // Reset sin6 -- sockaddr_in6
   sin6len = sizeof(struct sockaddr_in6);
   memset(&sin6, 0, sizeof(struct sockaddr_in6));
   sin6.sin6_port = htons(0); // 0 means anyone
   sin6.sin6_family = AF_INET6;
   sin6.sin6_addr = in6addr_any;

   // Bind
   status = bind(sock, (struct sockaddr *)&sin6, sin6len);
   if(-1 == status)
     perror("bind"), exit(1);

   memset(&sainfo, 0, sizeof(struct addrinfo));
   memset(&sin6, 0, sin6len);

   // Translate the name service of location and port to a set of socket address
   sainfo.ai_flags = 0;
   sainfo.ai_family = PF_INET6;
   sainfo.ai_socktype = SOCK_DGRAM;
   sainfo.ai_protocol = IPPROTO_UDP;
   status = getaddrinfo(argv[2], argv[3], &sainfo, &psinfo);

   switch (status)
     {
      case EAI_FAMILY: printf("family\n");
        break;
      case EAI_SOCKTYPE: printf("stype\n");
        break;
      case EAI_BADFLAGS: printf("flag\n");
        break;
      case EAI_NONAME: printf("noname\n");
        break;
      case EAI_SERVICE: printf("service\n");
        break;
     }

   //Set the text which will be sent
   sprintf(buffer,"This is testing packet.");

   status = sendto(sock, buffer, strlen(buffer), 0, (struct sockaddr *)psinfo->ai_addr, sin6len);  // status is how many bytes are sent successfully
   printf("buffer : %s \t%d\n", buffer, status);

   ///recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *)psinfo->ai_addr, sin6len);
   //printf("Receive from server: %s\n", buffer);


   // free memory
   freeaddrinfo(psinfo);
   psinfo = NULL;

   shutdown(sock, 2);
   close(sock);
   return 0;
}
