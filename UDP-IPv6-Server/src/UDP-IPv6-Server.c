/*
 ============================================================================
 Name        : UDP-IPv6.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

/* $Id$
 *
 *
 *
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#define MAXBUF 65536
#define SERV_PORT 55555

int main()
{
   int sock;  // socket descriptor
   int status;
   struct sockaddr_in6 sin6; // IPv6 address struct
   //int sin6len;
   char buffer[MAXBUF];
   char client_addr_str[64];

   // Create socket with IPv6 protocols(family), Datagram socket(type), and UDP transport protocol(protocol).
   // (sock is a socket descriptor)
   sock = socket(PF_INET6, SOCK_DGRAM,IPPROTO_UDP);

   //Converts 16-bit (2-byte) quantities from host byte order to network byte order.
   sin6.sin6_port = htons(SERV_PORT);
   sin6.sin6_family = AF_INET6;
   sin6.sin6_addr = in6addr_any;  // in6addr_any means all of network card and all of ip address
   status = bind(sock, (struct sockaddr *)&sin6, sizeof(sin6));

   if(status == -1) // returning 0 is successful
     perror("bind"), exit(1);
   printf("Server is started - Port Num is: %d\n", ntohs(sin6.sin6_port));

   //Start to receive message
   while(1){
	   status = recvfrom(sock, buffer, MAXBUF, 0,(struct sockaddr *)&sin6, sizeof(sin6));

	   //inet_ntop(PF_INET6, &(sin6.sin6_addr), client_addr_str, INET6_ADDRSTRLEN);
	   //printf("Received packet from %s:%d\n", client_addr_str, ntohs(sin6.sin6_port));
	   printf("Data: %s, and data length: %d\n\n", buffer, status);

	   //sendto(sock,"Received", 8 , 0, (struct sockaddr *)&sin6, sizeof(sin6));
   }

   //Shutdown
   shutdown(sock, 2);
   close(sock);
   return 0;
}
